<?php

//remove default controls
remove_theme_support( 'custom-background' );
remove_theme_support( 'custom-header' );
remove_theme_support( 'colors' );

/**
 * Add the theme configuration
 */
wstCustomizer::add_config( 'spalatobeans', array(
	'option_type' => 'theme_mod',
	'capability'  => 'edit_theme_options',
) );

/**
 * Registers and enqueues the `customizer-preview.js` file responsible
 * for handling the transport messages for the Theme Customizer.
 *
 */
function wst_customizer_live_preview() {

	wp_enqueue_script(
		'wst-theme-customizer',
		CHILD_JS . 'customizer-preview.js',
		array( 'jquery', 'customize-preview' ),
		'1.0.0',
		true
	);

}

add_action( 'customize_preview_init', 'wst_customizer_live_preview' );

/*-----------------------------------------------------------
	PANELS
/*------------------------------------------------------------*/


wstCustomizer::add_section( 'width', array(
	'title'      => esc_attr__( 'Widths', CHILD_TEXT_DOMAIN ),
	'priority'   => 1,
	'capability' => 'edit_theme_options',
) );

wstCustomizer::add_panel( 'background', array(
	'title'      => esc_attr__( 'Layout Backgrounds', CHILD_TEXT_DOMAIN ),
	'priority'   => 2,
	'capability' => 'edit_theme_options',
) );

wstCustomizer::add_section( 'header', array(
	'title'      => esc_attr__( 'Header', CHILD_TEXT_DOMAIN ),
	'priority'   => 5,
	'capability' => 'edit_theme_options',
) );

wstCustomizer::add_panel( 'typography', array(
	'title'      => esc_attr__( 'Typography', CHILD_TEXT_DOMAIN ),
	'priority'   => 3,
	'capability' => 'edit_theme_options',
) );
wstCustomizer::add_section( 'sidebar', array(
	'title'      => esc_attr__( 'Sidebar Colors', CHILD_TEXT_DOMAIN ),
	'priority'   => 4,
	'capability' => 'edit_theme_options',
) );
wstCustomizer::add_panel( 'blocks', array(
	'title'      => esc_attr__( 'Blocks', CHILD_TEXT_DOMAIN ),
	'priority'   => 7,
	'capability' => 'edit_theme_options',
) );
wstCustomizer::add_section( 'link_colors', array(
	'title'      => esc_attr__( 'Links and Buttons Colors', CHILD_TEXT_DOMAIN ),
	'priority'   => 6,
	'capability' => 'edit_theme_options',
) );





/*-----------------------------------------------------------
	SECTIONS
/*------------------------------------------------------------*/
//Backgrounds
wstCustomizer::add_section( 'general_bg', array(
	'title'      => esc_attr__( 'General Background', CHILD_TEXT_DOMAIN ),
	'priority'   => 1,
	'panel'      => 'background',
	'capability' => 'edit_theme_options',
) );

wstCustomizer::add_section( 'post_bg', array(
	'title'      => esc_attr__( 'Post Background', CHILD_TEXT_DOMAIN ),
	'priority'   => 2,
	'panel'      => 'background',
	'capability' => 'edit_theme_options',
) );
wstCustomizer::add_section( 'sidebar_bg', array(
	'title'      => esc_attr__( 'sidebar Background', CHILD_TEXT_DOMAIN ),
	'priority'   => 3,
	'panel'      => 'background',
	'capability' => 'edit_theme_options',
) );
wstCustomizer::add_section( 'header_bg', array(
	'title'      => esc_attr__( 'Header Background', CHILD_TEXT_DOMAIN ),
	'priority'   => 4,
	'panel'      => 'background',
	'capability' => 'edit_theme_options',
) );

wstCustomizer::add_section( 'off_canvas', array(
	'title'      => esc_attr__( 'Off canvas menu colors', CHILD_TEXT_DOMAIN ),
	'priority'   => 5,
	'panel'      => 'background',
	'capability' => 'edit_theme_options',
) );

wstCustomizer::add_section( 'footer_bg', array(
	'title'      => esc_attr__( 'footer Background', CHILD_TEXT_DOMAIN ),
	'priority'   => 6,
	'panel'      => 'background',
	'capability' => 'edit_theme_options',
) );

//typo
wstCustomizer::add_section( 'body_typo', array(
	'title'      => esc_attr__( 'Body Typography', CHILD_TEXT_DOMAIN ),
	'priority'   => 1,
	'panel'      => 'typography',
	'capability' => 'edit_theme_options',
) );
wstCustomizer::add_section( 'headings_typo', array(
	'title'      => esc_attr__( 'Headings Typography', CHILD_TEXT_DOMAIN ),
	'priority'   => 2,
	'panel'      => 'typography',
	'capability' => 'edit_theme_options',
) );
wstCustomizer::add_section( 'sidebar_typo', array(
	'title'      => esc_attr__( 'Sidebar Typography', CHILD_TEXT_DOMAIN ),
	'priority'   => 3,
	'panel'      => 'typography',
	'capability' => 'edit_theme_options',
) );
wstCustomizer::add_section( 'header_typo', array(
	'title'      => esc_attr__( 'Header Typography', CHILD_TEXT_DOMAIN ),
	'priority'   => 4,
	'panel'      => 'typography',
	'capability' => 'edit_theme_options',
) );
wstCustomizer::add_section( 'footer_typo', array(
	'title'      => esc_attr__( 'Footer Typography', CHILD_TEXT_DOMAIN ),
	'priority'   => 5,
	'panel'      => 'typography',
	'capability' => 'edit_theme_options',
) );
//Blocks
wstCustomizer::add_section( 'tabs', array(
	'title'      => esc_attr__( 'Tabs', CHILD_TEXT_DOMAIN ),
	'priority'   => 5,
	'panel'      => 'blocks',
	'capability' => 'edit_theme_options',
) );
wstCustomizer::add_section( 'gallery', array(
	'title'      => esc_attr__( 'Gallery', CHILD_TEXT_DOMAIN ),
	'priority'   => 5,
	'panel'      => 'blocks',
	'capability' => 'edit_theme_options',
) );


/*-----------------------------------------------------------
	TYPOGRAPHY CONTROLS
/*------------------------------------------------------------*/

/**
 * Add the body-typography control
 */
wstCustomizer::add_field( 'spalatobeans', array(
	'type'        => 'typography',
	'settings'    => 'body_typography',
	'label'       => esc_attr__( 'Body Typography', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Select the main typography options for your site.', CHILD_TEXT_DOMAIN ),
	'help'        => esc_attr__( 'The typography options you set here apply to all content on your site.',
		CHILD_TEXT_DOMAIN ),
	'section'     => 'body_typo',
	'transport'   => 'auto',
	'priority'    => 1,
	'default'     => array(
		'font-family'    => 'Raleway',
		'variant'        => '400',
		'font-size'      => '16px',
		'line-height'    => '1.5',
		'letter-spacing' => '0',
		'color'          => '#444',
	),

	'output' => array(
		array(
			'element' => 'html, .uk-panel-box, .tm-secondary .tm-widget .uk-list li a ',
		),
	),
) );

wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'typography',
	'settings'  => 'meta_typography',
	'label'     => esc_attr__( 'Post Meta Typography', CHILD_TEXT_DOMAIN ),
	'section'   => 'body_typo',
	'transport' => 'auto',
	'priority'  => 1,
	'default'   => array(
		'font-family'    => 'Raleway',
		'variant'        => '400',
//		'font-size'      => '12px',
		'line-height'    => '1.5',
		'letter-spacing' => '0',
		'color'          => '#999',
	),

	'output' => array(
		array(
			'element' => '.uk-article-meta, .uk-article-meta time, .uk-article-meta time a , .uk-article-meta .uk-subnav > * > * ',
		),
	),
) );

/**
 * Add the heading-typography control
 */
wstCustomizer::add_field( 'spalatobeans', array(
	'type'        => 'typography',
	'settings'    => 'headings_typography',
	'label'       => esc_attr__( 'Headings Typography', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Select the typography options for your headings.', CHILD_TEXT_DOMAIN ),
	'section'     => 'headings_typo',
	'transport'   => 'auto',
	'priority'    => 2,
	'default'     => array(
		'font-family'    => 'Oswald',
		'variant'        => '400',
//		 'font-size'      => '16px',
		'line-height'    => '1.5',
		'letter-spacing' => '0',
		'color'          => '#444',
		'text-transform' => 'none',
	),

	'output' => array(
		array(
			'element' => array(
				'h1',
				'h2',
				'h3',
				'h4',
				'h5',
				'h6',
				'.h1',
				'.h2',
				'.h3',
				'.h4',
				'.h5',
				'.h6',
				'h1 a',
				'h2 a',
				'h3 a',
				'h4 a',
				'h5 a',
				'h6 a',
				'uk-form legend','legend'

			),
		),
	),
) );

wstCustomizer::add_field( 'spalatobeans', array(
	'type'        => 'typography',
	'settings'    => 'sidebar_typography',
	'label'       => esc_attr__( 'Sidebar Typography', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Select the sidebar typography options for your site.', CHILD_TEXT_DOMAIN ),
	'section'     => 'sidebar_typo',
	'transport'   => 'auto',
	'priority'    => 3,
	'default'     => array(
		'font-family'    => 'Raleway',
		'variant'        => '400',
		'font-size'      => '16px',
		'line-height'    => '1.5',
		'letter-spacing' => '0',
		'color'          => '#444',
	),

	'output' => array(
		array(
			'element' => '.tm-secondary .tm-widget .uk-list li a ',
		),
	),
) );

wstCustomizer::add_field( 'spalatobeans', array(
	'type'        => 'typography',
	'settings'    => 'sidebar_headings_typography',
	'label'       => esc_attr__( 'Sidebar Headings Typography', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Select the sidebar Headings typography options for your site.', CHILD_TEXT_DOMAIN ),
	'section'     => 'sidebar_typo',
	'transport'   => 'auto',
	'priority'    => 3,
	'default'     => array(
		'font-family'    => 'Oswald',
		'variant'        => '400',
		'font-size'      => '16px',
		'line-height'    => '1.5',
		'letter-spacing' => '0',
		'color'          => '#fff',
	),

	'output' => array(
		array(
			'element' => '.tm-secondary .tm-widget .uk-panel-title ',
		),
	),
) );

/**
 * Navigation typography
 */
wstCustomizer::add_field( 'spalatobeans', array(
	'type'        => 'typography',
	'settings'    => 'nav_typography',
	'label'       => esc_attr__( 'Header Navigation Typography', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Select the navigations typography options for your site.', CHILD_TEXT_DOMAIN ),
	'section'     => 'header_typo',
	'transport'   => 'auto',
	'priority'    => 3,
	'default'     => array(
		'font-family'    => 'Oswald',
		'variant'        => '400',
		'font-size'      => '12px',
		'line-height'    => '40px',
		'color'          => '#444',
		'letter-spacing' => '0',
		'text-transform' => 'uppercase',
	),

	'output' => array(
		array(
			'element' => '.tm-header .uk-navbar-nav > li > a, .tm-header .sub-menu > li > a',
		),
	),
) );

wstCustomizer::add_field( 'spalatobeans', array(
	'type'        => 'typography',
	'settings'    => 'top_bar_typography',
	'label'       => esc_attr__( 'Top Bar Typography', CHILD_TEXT_DOMAIN ),
	'section'     => 'header_typo',
	'transport'   => 'auto',
	'priority'    => 3,
	'default'     => array(
		'font-family'    => 'Oswald',
		'variant'        => '400',
		'font-size'      => '16px',
		'line-height'    => '1.5',
		'color'          => 'red',
		'letter-spacing' => '0',
		'text-transform' => 'uppercase',
	),

	'output' => array(
		array(
			'element' => ' .top-bar a:not(.uk-icon-button) , .top-bar, .top-bar p',
		),
	),
) );


/**
 * off canvas typography
 */
wstCustomizer::add_field( 'spalatobeans', array(
	'type'        => 'typography',
	'settings'    => 'off_canvas_typography',
	'label'       => esc_attr__( 'Off canvas menu Typography', CHILD_TEXT_DOMAIN ),
	'section'     => 'header_typo',
	'transport'   => 'auto',
	'priority'    => 3,
	'default'     => array(
		'font-family'    => 'Oswald',
		'variant'        => '400',
		'font-size'      => '16px',
		'line-height'    => '1.5',
		'color'          => '#fff',
		'letter-spacing' => '0',
		'text-transform' => 'uppercase',
	),

	'output' => array(
		array(
			'element' => '.uk-nav-offcanvas > li > a, .uk-nav-offcanvas ul a',
		),
	),
) );


//site branding
wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'typography',
	'settings'  => 'logo_typography',
	'label'     => esc_attr__( 'Site branding Typography', CHILD_TEXT_DOMAIN ),
	'section'   => 'header_typo',
	'transport' => 'auto',
	'priority'  => 4,
	'default'   => array(
		'font-family'    => 'Oswald',
		'variant'        => '400',
		'font-size'      => '18px',
		'line-height'    => '40px',
		'color'          => '#444',
		'letter-spacing' => '0',
		'text-transform' => 'uppercase',
	),

	'output' => array(
		array(
			'element' => '.tm-site-branding a',
		),
	),
) );

//footer
wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'typography',
	'settings'  => 'fat_footer_typo',
	'label'     => esc_attr__( 'Fat Footer Typography', CHILD_TEXT_DOMAIN ),
	'section'   => 'footer_typo',
	'transport' => 'auto',
	'priority'  => 1,
	'default'   => array(
		'font-family'    => 'raleway',
		'variant'        => '400',
		'font-size'      => '16px',
		'line-height'    => '1.5',
		'color'          => '#fff',
		'letter-spacing' => '0',
		'text-transform' => '',
	),

	'output' => array(
		array(
			'element' => '.tm-fat-footer',
		),
	),
) );


wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'typography',
	'settings'  => 'fat_footer_headings_typography',
	'label'     => esc_attr__( 'Fat Footer Headings Typography', CHILD_TEXT_DOMAIN ),
	'section'   => 'footer_typo',
	'transport' => 'auto',
	'priority'  => 4,
	'default'   => array(
		'font-family'    => 'Oswald',
		'variant'        => '400',
		'font-size'      => '18px',
		'line-height'    => '1.5',
		'color'          => '#E72564',
		'letter-spacing' => '0',
		'text-transform' => 'uppercase',
	),

	'output' => array(
		array(
			'element' => '.tm-fat-footer .tm-widget .uk-panel-title',
		),
	),
) );

wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'typography',
	'settings'  => 'footer_typo',
	'label'     => esc_attr__( 'Footer Typography', CHILD_TEXT_DOMAIN ),
	'section'   => 'footer_typo',
	'transport' => 'auto',
	'priority'  => 1,
	'default'   => array(
		'font-family'    => 'raleway',
		'variant'        => '400',
		'font-size'      => '16px',
		'line-height'    => '1.5',
		'color'          => '#fff',
		'letter-spacing' => '0',
		'text-transform' => '',
	),

	'output' => array(
		array(
			'element' => '.tm-footer',
		),
	),
) );
/*-----------------------------------------------------------
	BG CONTROLS
/*------------------------------------------------------------*/
wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'background',
	'settings'  => 'general_background',
	'label'     => esc_attr__( 'General Background', CHILD_TEXT_DOMAIN ),
	'priority'  => 1,
	'section'   => 'general_bg',
	'default'   => array(
		'background-color'      => '#fafafa',
		'background-image'      => '',
		'background-repeat'     => 'repeat',
		'background-position'   => 'center center',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => 'html, .tm-main',
			'property' => 'background'
		)
	)
) );


wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'background',
	'settings'  => 'post_background',
	'label'     => esc_attr__( 'Post Background', CHILD_TEXT_DOMAIN ),
	'priority'  => 2,
	'section'   => 'post_bg',
	'default'   => array(
		'background-color'      => '#fafafa',
		'background-image'      => '',
		'background-repeat'     => 'repeat',
		'background-position'   => 'center center',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.uk-article',
			'property' => 'background'
		)
	)
) );

wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'background',
	'settings'  => 'sidebar_background',
	'label'     => esc_attr__( 'Sidebar Background', CHILD_TEXT_DOMAIN ),
	'priority'  => 3,
	'section'   => 'sidebar_bg',
	'default'   => array(
		'background-color'      => '#ffffff',
		'background-image'      => '',
		'background-repeat'     => 'repeat',
		'background-position'   => 'center center',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.tm-secondary .uk-panel',
			'property' => 'background'
		)
	)
) );
wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'background',
	'settings'  => 'sidebar_headings_backround',
	'label'     => esc_attr__( 'Sidebar headings Background', CHILD_TEXT_DOMAIN ),
	'priority'  => 5,
	'section'   => 'sidebar_bg',
	'default'   => array(
		'background-color'      => '#E72564',
		'background-image'      => '',
		'background-repeat'     => 'repeat',
		'background-position'   => 'center center',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.tm-secondary .tm-widget .uk-panel-title',
			'property' => 'background'
		)
	)
) );


wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'background',
	'settings'  => 'header_background',
	'label'     => esc_attr__( 'Header Background', CHILD_TEXT_DOMAIN ),
	'priority'  => 4,
	'section'   => 'header_bg',
	'default'   => array(
		'background-color'      => '#fff',
		'background-image'      => '',
		'background-repeat'     => 'repeat',
		'background-position'   => 'center center',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.tm-header,.tm-primary-menu .uk-dropdown, .tm-header .uk-container',
			'property' => 'background'
		)
	)
) );

wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'background',
	'settings'  => 'top_bar_background',
	'label'     => esc_attr__( 'top Bar Background', CHILD_TEXT_DOMAIN ),
	'priority'  => 5,
	'section'   => 'header_bg',
	'default'   => array(
		'background-color'      => '#2D3146',
		'background-image'      => '',
		'background-repeat'     => 'repeat',
		'background-position'   => 'center center',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.top-bar',
			'property' => 'background'
		)
	)
) );

wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'background',
	'settings'  => 'fat_footer_background',
	'label'     => esc_attr__( 'Fat Footer Background', CHILD_TEXT_DOMAIN ),
	'priority'  => 1,
	'section'   => 'footer_bg',
	'default'   => array(
		'background-color'      => '#2D3146',
		'background-image'      => '',
		'background-repeat'     => 'repeat',
		'background-position'   => 'center center',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.tm-fat-footer, .tm-fat-footer .uk-panel-box, .tm-fat-footer .uk-container',
			'property' => 'background'
		)
	)
) );

wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'background',
	'settings'  => 'footer_background',
	'label'     => esc_attr__( 'Footer Background', CHILD_TEXT_DOMAIN ),
	'priority'  => 1,
	'section'   => 'footer_bg',
	'default'   => array(
		'background-color'      => '#242637',
		'background-image'      => '',
		'background-repeat'     => 'repeat',
		'background-position'   => 'center center',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.tm-footer, .tm-footer .uk-container, .tm-footer .tm-widget',
			'property' => 'background'
		)
	)
) );

wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'color',
	'settings'  => 'off_canvas_background',
	'label'     => esc_attr__( 'Off Canvas Background', CHILD_TEXT_DOMAIN ),
	'priority'  => 1,
	'section'   => 'off_canvas',
	 'default'   => "#2D3146",
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.uk-offcanvas-bar',
			'property' => 'background'
		)
	)
) );

wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'color',
	'settings'  => 'off_canvas_hover',
	'label'     => esc_attr__( 'Off Canvas hover Background', CHILD_TEXT_DOMAIN ),
	'priority'  => 1,
	'section'   => 'off_canvas',
	'default'   => "#E72564",
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.uk-nav-offcanvas > .uk-open > a, html:not(.uk-touch) .uk-nav-offcanvas > li > a:hover, html:not(.uk-touch) .uk-nav-offcanvas > li > a:focus,html .uk-nav.uk-nav-offcanvas > li.uk-active > a',
			'property' => 'background'
		)
	)
) );



/*-----------------------------------------------------------
	WIDTH CONTROLS
/*------------------------------------------------------------*/
wstCustomizer::add_field( 'spalatobeans', array(
	'type'        => 'checkbox',
	'settings'    => 'boxed',
	'label'       => esc_attr__( 'Boxed layout', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Check this if you want a boxed layout', CHILD_TEXT_DOMAIN ),
	'section'     => 'width',
	'transport'   => 'postMessage',
	'default'     => 0,
) );

/*-----------------------------------------------------------
	HEADER CONTROLS
/*------------------------------------------------------------*/
wstCustomizer::add_field( 'spalatobeans', array(
	'type'        => 'checkbox',
	'settings'    => 'center_header',
	'label'       => esc_attr__( 'Center Header', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Check this if you want to center logo and menu' ),
	'section'     => 'header',
	'default'     => 0,
	'transport'   => 'postMessage',

) );

wstCustomizer::add_field( 'spalatobeans', array(
	'type'        => 'checkbox',
	'settings'    => 'transparent_header',
	'label'       => esc_attr__( 'Transparent Header', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Check this if you want a transparent header' ),
	'section'     => 'header',
	'default'     => 1,
	'transport'   => 'postMessage',

) );

wstCustomizer::add_field( 'spalatobeans', array(
	'type'        => 'checkbox',
	'settings'    => 'sticky_header',
	'label'       => esc_attr__( 'Sticky Header', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Check this if you want a sticky header' ),
	'section'     => 'header',
	'default'     => 1,
	'transport'   => 'postMessage',

) );

/*-----------------------------------------------------------
	TABS
/*------------------------------------------------------------*/
wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'color',
	'settings'  => 'tab_content_bg',
	'label'     => esc_attr__( 'Content Background Color', CHILD_TEXT_DOMAIN ),
	'section'   => 'tabs',
	'default'   => "#f5f5f5",
	'priority'  => 1,
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.tab-content',
			'property' => 'background',
		)
	),

) );


wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'color',
	'settings'  => 'tab_title_bg',
	'label'     => esc_attr__( 'Tab Title Background Color', CHILD_TEXT_DOMAIN ),
	'section'   => 'tabs',
	'default'   => "#E52B65",
	'priority'  => 3,
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.tab-title',
			'property' => 'background',
		)
	),

) );
wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'color',
	'settings'  => 'tab_title_bg_hover',
	'label'     => esc_attr__( 'Tab Title Background Color Hover and Active color', CHILD_TEXT_DOMAIN ),
	'section'   => 'tabs',
	'default'   => "#E52B65",
	'priority'  => 4,
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.tab-title:hover, .tab-title.uk-active',
			'property' => 'background',
		)
	),

) );

/*-----------------------------------------------------------
	GALLERY
/*------------------------------------------------------------*/

wstCustomizer::add_field( 'spalatobeans', array(
	'type'  => 'color',
	'choices'     => array(
		'alpha' => true,
	),
	'settings'  => 'overlay',
	'label'     => esc_attr__( 'Gallery Overlay color', CHILD_TEXT_DOMAIN ),
	'section'   => 'gallery',
	'default'   => "rgba(0,0,0, .3)",
	'priority'  => 1,
	'transport' => 'auto',
	'output'    => array(
	array(
		'element'  => '.gallery-item .uk-overlay-background',
		'property' => 'background',
	)
),

) );

wstCustomizer::add_field( 'spalatobeans', array(
	'type'      => 'typography',
	'settings'  => 'overlay_typo',
	'label'     => esc_attr__( 'Gallery Overlay Typography', CHILD_TEXT_DOMAIN ),
	'section'   => 'gallery',
	'transport' => 'auto',
	'priority'  => 1,
	'default'   => array(
		'font-family'    => 'Oswald',
		'variant'        => '400',
		'font-size'      => '18px',
		'line-height'    => '1.5',
		'color'          => '#fff',
		'letter-spacing' => '0',
		'text-transform' => 'uppercase',
	),

	'output' => array(
		array(
			'element' => '.uk-overlay-panel',
		),
	),
) );

/*-----------------------------------------------------------
	LINKS AND BUTTONS
/*------------------------------------------------------------*/

wstCustomizer::add_field( 'spalatobeans', array(
	'type'  => 'color',
	'choices'     => array(
		'alpha' => true,
	),
	'settings'  => 'link_buttons',
	'label'     => esc_attr__( 'Links and Buttons colors', CHILD_TEXT_DOMAIN ),
	'section'   => 'link_colors',
	'default'   => "#E72564",
	'priority'  => 1,
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => 'a, time, .uk-button, .tm-header.uk-active .uk-navbar-nav > li > a:hover, .tm-header .uk-navbar-nav > li > a:hover, .checked li:before, .uk-button',
			'property' => 'color',
		),
		array(
			'element'  => '.uk-button',
			'property' => 'border-color',
		),
		array(
			'element'  => '.uk-button-primary, .uk-pagination > li > a:hover, .tm-footer .uk-icon-button, .top-bar .tm-widget .uk-icon-button, .uk-nav-dropdown > li > a:hover, .uk-nav-dropdown > li > a:focus ,.uk-pagination > .uk-active > span',
			'property' => 'background',
		)
	),

) );

wstCustomizer::add_field( 'spalatobeans', array(
	'type'  => 'color',
	'choices'     => array(
		'alpha' => true,
	),
	'settings'  => 'link_buttons_hover',
	'label'     => esc_attr__( 'Links and Buttons hover colors', CHILD_TEXT_DOMAIN ),
	'section'   => 'link_colors',
	'default'   => "#4b5076",
	'priority'  => 2,
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => 'a:hover, .uk-subnav > * > :hover, .uk-subnav > * > :focus',
			'property' => 'color',
		),
		array(
			'element'  => '.uk-button-primary:hover, .uk-pagination > li > a, .tm-footer .uk-icon-button:hover, .top-bar .tm-widget .uk-icon-button:hover, uk-button:hover, uk-button:focus',
			'property' => 'background-color',
		),
		array(
			'element'  => 'uk-button:hover',
			'property' => 'border-color',
		),
	),

) );

/*-----------------------------------------------------------
	OFF CANVAS
/*------------------------------------------------------------*/




