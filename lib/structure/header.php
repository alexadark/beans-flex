<?php
add_action( 'wp', 'wst_set_up_header_structure' );
/**
 * Set up header structure
 *
 * @since 1.0.0
 *
 * @return void
 */
function wst_set_up_header_structure() {

    //Remove title tagline
	beans_remove_action( 'beans_site_title_tag' );

	if(get_theme_mod('sticky_header',1) ) {
		//sticky header
		beans_add_attribute( 'beans_header', 'data-uk-sticky', "{top:-300, animation:'uk-animation-slide-top'}" );
	}

    //Hero area
	add_action( 'beans_header_after_markup', 'wst_display_hero_area' );


	// Breadcrumb
	beans_remove_action( 'beans_breadcrumb' );

	//top bar
	add_action( 'beans_header_before_markup', 'wst_display_top_bar' );
    beans_remove_attribute('beans_widget_panel_top-bar','class','uk-panel-box');
    beans_add_attribute('beans_widget_panel_top-bar','class','uk-hidden-small');

	if ( get_theme_mod( 'center_header', 0) ) {
		beans_remove_attribute( 'beans_site_branding', 'class', 'uk-float-left' );
		beans_replace_attribute( 'beans_primary_menu', 'class', 'uk-float-right', 'uk-text-center' );
		beans_add_attribute( 'beans_menu[_navbar]', 'class', 'uk-display-inline-block' );
		beans_add_attribute( 'beans_menu_item', 'class', 'uk-text-left' );

//		add_filter('body_class','wst_centered_header');
        beans_add_attribute('beans_body','class','centered-header');

	}


	if(get_field('builder_transparent_header')){
		beans_add_attribute('beans_body', 'class', 'builder-transparent-header');
	}

	$logo = get_field('custom_logo');
	$url = get_field('custom_logo_url');
	if($logo){

		beans_replace_attribute('beans_logo_image', 'src',esc_url($logo));
    }

    if($url){
	    beans_replace_attribute('beans_site_title_link','href',esc_url($url));
    }


}

if(get_theme_mod('transparent_header',1) ){
	beans_add_attribute('beans_body', 'class', 'transparent-header');
}

/**
 * Display top bar
 *
 * @since 1.0.0
 *
 * @return void
 */
function wst_display_top_bar(){

	if(!is_active_sidebar('top-bar')){
		return;
	} ?>

    <div class="top-bar">
        <div class="uk-container uk-container-center">
			<?php echo beans_widget_area( 'top-bar' ); ?>
        </div>
    </div>

<?php }

/**
 * Display hero area
 *
 * @since 1.0.0
 *
 * @return void
 */
function wst_display_hero_area() {

	if(is_home()||is_archive()){
		return;
	}
	$context   = Timber::get_context();
	$templates = array( 'hero.twig' );
	Timber::render( $templates, $context );


}

