<?php

add_action( 'wp', 'wst_set_up_post_structure' );
/**
 * Set up post structure
 *
 * @since 1.0.0
 *
 * @return void
 */
function wst_set_up_post_structure() {
	//Remove featured image (it will appear in hero area)
	beans_remove_action( 'beans_post_image' );

	beans_add_attribute('beans_post_meta_categories','class','uk-article-meta');

	//comments
	beans_remove_attribute( 'beans_comments', 'class', 'uk-panel-box' );
	beans_add_attribute( 'beans_comment_form_wrap', 'class', 'uk-panel-box' );

	//pages
	if ( ! is_page() ) {
		return;
	}
	//Remove title only on pages
	beans_remove_action( 'beans_post_title' );

	//remove comments from pages
	remove_post_type_support( 'page', 'comments' );




}
//Boxed layout option

if(get_theme_mod('boxed',0) ) {

	beans_add_attribute( 'beans_html', 'class', 'boxed' );

}

